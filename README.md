# framebunker.Nix.Space

NixOS configuration for full JetBrains Space service - based on the proof of concept release. Declarative options include network setup, email, paths, system user account, and optionally encryption keys file.

If not provided, the encryption keys file will be generated at specified (or default) path on first launch - based on service default key generation behaviour. Nix-driven config updates will thus result in the same keys getting re-applied as were used for the current service state data.

Note that config files will always be re-generated on launch.

## Updating

Existing installs based on the beta release will not have had keys properly extracted. To avoid losing access to existing data by trampling keys, please run `space-config-dump.sh <path>` before upgrading (where path is default `/etc/nixos/src/space-config-keys.json` or your intended keys path).

Further, make sure to set `services.jetbrains-space.betaCompatible` to `true` to ensure database compatibility in an area which appears to have changed from beta to release.

More reading available here ("Docker Compose installation" section): [JetBrains: How to migrate](https://www.jetbrains.com/help/space/migrate-from-earlier-versions.html#how-to-migrate).
