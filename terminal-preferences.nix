#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  defaultEditor = pkgs.micro;

  cfg = config.programs.preferred-terminal;
in
with lib;
{
  options.programs.preferred-terminal.enable = mkEnableOption "Preferred terminal setup";

  config = mkIf cfg.enable
  {
    programs.zsh = {
      enable = true;
      ohMyZsh.enable = true;
      promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
    };

    programs.thefuck.enable = true;

    environment = {
      systemPackages = with pkgs; [
        zsh-powerlevel10k
        tealdeer
        bat
        exa
        defaultEditor
      ];
      variables.EDITOR = getName defaultEditor;
      shellAliases = {
        edit = getName defaultEditor;
        nano = ''function f(){ echo "Use edit in stead"; unset -f f; }; f''; # Killing old habits
        cat = ''bat --paging=never --decorations=never'';
        ls = ''exa'';
        l = ''ls -al'';
        la = ''ls -al'';
        lsa = ''ls -al'';
      };
    };

    users.defaultUserShell = pkgs.zsh;
  };
}
