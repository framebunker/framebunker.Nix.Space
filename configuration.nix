#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ config, pkgs, ... }:
let
  users.main = {
    name = "";
    keyFiles = [];
  };

  network = {
    interface = "";
    host = "";
    domain = "";
    sshPort = 2222;
    ip = "";
  };

  email = {
    fromAddress = "";
    host = "";
    login = {
      user = "";
      password = "";
    };
  };

  timeZone = "Europe/Copenhagen";
  rebootHour = "07";
  rebootMinute = "37";
in
{
  imports = [
    ./hardware-configuration.nix
    ./terminal-preferences.nix
    ./daily-reboot.nix
    ./jetbrains-space.nix
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernelParams = [
      "console=ttyS0,115200"
      "console=tty1"
    ];
    tmpOnTmpfs = true;
  };

  fileSystems."/packages".neededForBoot = true;
  services.qemuGuest.enable = true;
  zramSwap.enable = true;

  networking = {
    hostName = network.host;
    useDHCP = false;
    interfaces."${network.interface}".useDHCP = true;
  };

  time.timeZone = timeZone;
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  environment.systemPackages = with pkgs; [
    parted
    xfsprogs
  ];

  users.users = {
    "${users.main.name}" = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keyFiles = users.main.keyFiles;
    };
  };

  programs.preferred-terminal.enable = true;

  services.jetbrains-space = {
    enable = true;
    network = {
      dns = network.host + "." + network.domain;
      domain-name = network.domain;
      ip = network.ip;
    };
    email = email;
  };

  services.daily-reboot = {
    enable = true;
    hour = rebootHour;
    minute = rebootMinute;
  };

  services.openssh = {
    enable = true;
    permitRootLogin = "no";
    passwordAuthentication = false;
    kbdInteractiveAuthentication = false;
    ports = [ network.sshPort ];
  };

  networking.firewall.allowedTCPPorts = config.services.openssh.ports;

  system.copySystemConfiguration = true;

  system.stateVersion = "22.05";
}
