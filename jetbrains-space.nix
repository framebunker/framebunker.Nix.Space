#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

{ lib, pkgs, config, ... }:
let
  cfg = config.services.jetbrains-space;

  default-credentials = {
    user = "jetbrains-space";
    group = "jetbrains-space";
  };

  relay.ports = {
    http = 80;
    https = 443;
    ssh = 22;
  };
  networks = {
    frontend.name = "frontend";
    backend = {
      applications.name = "backend-applications";
      data.name = "backend-data";
    };
  };
  shared-config = {
    version = "2023.1.0";
    tag = "2023.1.0.684";
    path = cfg.paths.bulk + "/shared/config/";
    paths = {
      space = shared-config.path + "space.on-premises.conf";
      packages = shared-config.path + "packages.on-premises.conf";
      vcs = shared-config.path + "vcs.on-premises.properties";
      extract = cfg.paths.keys;
    };
    available-test-path = shared-config.paths.space;
  };
  space = {
    version = "2023.1.0";
    ports = {
      http = 8084;
    };
    credentials = {
      postgres = {
        user = "space";
        password = "spacepassword";
        database = "spacedb";
      };
      minio = {
        user = "space-access-key";
        password = "space-secret-key";
      };
    };
    paths.config = shared-config.path;
    betaCompatible = cfg.betaCompatible;
  };
  vcs = {
    version = "2023.1.0";
    ports = {
      http = 8080;
      ssh = 2222;
    };
    paths.config = shared-config.path;
  };
  packages = {
    version = "2023.1.0";
    ports = {
      serviceA = 8390;
      serviceB = 9390;
    };
    paths.config = shared-config.path;
  };
  langservice = {
    version = "2023.1.0";
    ports.service = 8095;
    paths.config = shared-config.path;
  };
  postgres = {
    version = "12.2";
    ports.service = 5432;
    paths.data = cfg.paths.bulk + "/postgres/data/";
  };
  elasticsearch = {
    version = "7.17.7";
    ports = {
      serviceA = 9200;
      serviceB = 9300;
    };
    paths.data = cfg.paths.bulk + "/elasticsearch/data/";
  };
  redis = {
    version = "7.0.2-alpine";
    ports.service = 6379;
  };
  minio = {
    version = "RELEASE.2021-09-09T21-37-07Z";
    ports = {
      service = 9000;
      console = 9001;
    };
    paths.data = cfg.paths.bulk + "/minio/data/";
  };

  configExtractScript = {
    name = "jetbrains-space-extract-keys";
    package = pkgs.writeScriptBin configExtractScript.name (''
      mkdir -p /tmp/${configExtractScript.name}/
      cd /tmp/${configExtractScript.name}/

      if [[ -f ${shared-config.paths.extract} && $1 != "-f" ]]; then
        echo "Space config dump found at ${shared-config.paths.extract}, force (-f) flag not provided. Exiting extractor."
        exit 1
      fi

      echo "Dumping space config to ${shared-config.paths.extract}..."
      /etc/nixos/space-config-dump.sh ${shared-config.path} > ${shared-config.paths.extract}
      echo "Done"
    '');
  };

  configScript = {
    name = "jetbrains-space-regenerate-config";
    package = pkgs.writeScriptBin configScript.name (''
      wait_for_file()
      {
        echo -n "Wait for file $1"
        while [ ! -f "$1" ]; do
          sleep 0.5 && echo -n .
        done
        echo
      }


      read_key()
      {
        local key=$1

        # Read raw string value
        local value=$(${pkgs.jq}/bin/jq -rM ".$key" < "${shared-config.paths.extract}")

        # Escape value for use in sed arguments
        value=''${value//\\/\\\\}
        value=''${value//\//\\\/}

        echo $value
      }
      
    
      echo "Clearing any existing configuration"
      rm ${shared-config.path}*.* 2> /dev/null || true
    
      echo "Running initial configuration..."
      systemctl start --wait podman-init-config

      wait_for_file ${shared-config.paths.space}
      wait_for_file ${shared-config.paths.vcs}

      echo "Applying nix-configured settings"

      sed -zi \
        -e 's/'' +
          ''mail {\n'' +
          ''[[:space:]]\{,\}outgoing {\n''+
          ''[[:space:]]\{,\}enabled = false\n\n[[:space:]]\{,\}\/\/ SMTP settings\n'' + 
          ''[[:space:]]\{,\}fromAddress = "space@space.example.com"\n'' + 
          ''[[:space:]]\{,\}host = "mail"\n'' + 
          ''[[:space:]]\{,\}port = 25\n'' + 
          ''[[:space:]]\{,\}protocol = "SMTP"\n'' + 
          ''[[:space:]]\{,\}login = "space"\n'' + 
          ''[[:space:]]\{,\}password = "space"'' +
        ''/'' + 
          ''mail{ outgoing {\nenabled = true\n'' + 
          ''fromAddress = "${cfg.email.fromAddress}"\n'' + 
          ''host = "${cfg.email.host}"\n'' + 
          ''port = ${toString cfg.email.port}\n'' + 
          ''protocol = "${cfg.email.protocol}"\n'' +
          ''login = "${cfg.email.login.user}"\n'' +
          ''password = "${cfg.email.login.password}"\n'' +
        ''/' \
        ${shared-config.paths.space}'' +
      ''

      sed -zi \
        -e 's/frontend {\n[[:space:]]\{,\}url = "http:\/\/localhost:8084/frontend {\nurl = "http:\/\/space.${cfg.network.domain-name}/' \
        ${shared-config.paths.space}

      sed -zi \
        -e 's/base.url=http:\/\/localhost:/base.url=http:\/\/space.${cfg.network.domain-name}:/' \
        -e 's/circlet.url.ext=http:\/\/localhost:/circlet.url.ext=http:\/\/space.${cfg.network.domain-name}:/' \
        ${shared-config.paths.vcs}

      if [[ ! -f "${shared-config.paths.extract}" ]]; then
        echo "No keys file found at ${shared-config.paths.extract}. Sticking with newly generated keys."
        exit
      fi

      echo "Applying keys from ${shared-config.paths.extract}"

      SPACE_MASTER_SECRET=$(read_key "config_space.circlet.masterSecret")
      SPACE_OAUTH_ACCESS_TOKEN_RSA_PUBLIC=$(read_key "config_space.circlet.oauth.accessToken.rsa.public")
      SPACE_OAUTH_ACCESS_TOKEN_RSA_PRIVATE=$(read_key "config_space.circlet.oauth.accessToken.rsa.private")
      SPACE_OAUTH_MESSAGE_ENCODING_KEY=$(read_key "config_space.circlet.oauth.message.encoding.key")
      SPACE_OAUTH_2FA_ENCODING_KEY=$(read_key "config_space.circlet.oauth.twoFactor.encryptionKey")
      SPACE_OAUTH_ENCODING_KEY=$(read_key "config_space.circlet.oauth.encryptionKey")
      SPACE_OAUTH_MESSAGE_SIGNING_RSA_PUBLIC=$(read_key "config_space.circlet.oauth.messageSigning.rsa.public")
      SPACE_OAUTH_MESSAGE_SIGNING_RSA_PRIVATE=$(read_key "config_space.circlet.oauth.messageSigning.rsa.private")
      SPACE_VCS_SHARED_KEY=$(read_key "config_space.circlet.vcs.secret")
      VCS_IDENTITY_EMAIL=$(read_key "config_space.circlet.vcs.identity.email")
      VCS_IDENTITY_SECRETKEY_BASE64=$(read_key "config_space.circlet.vcs.identity.secretKey_base64")
      SPACE_WEBHOOK_KEY=$(read_key "config_space.circlet.webhooks.key")
      SPACE_PACKAGES_SHARED_CLIENT_SECRET=$(read_key "config_space.circlet.packages.oauth.clientSecret")

      sed -i \
        -e "s/%SPACE_MASTER_SECRET%/$SPACE_MASTER_SECRET/" \
        -e "s/%SPACE_OAUTH_ACCESS_TOKEN_RSA_PUBLIC%/$SPACE_OAUTH_ACCESS_TOKEN_RSA_PUBLIC/" \
        -e "s/%SPACE_OAUTH_ACCESS_TOKEN_RSA_PRIVATE%/$SPACE_OAUTH_ACCESS_TOKEN_RSA_PRIVATE/" \
        -e "s/%SPACE_OAUTH_MESSAGE_ENCODING_KEY%/$SPACE_OAUTH_MESSAGE_ENCODING_KEY/" \
        -e "s/%SPACE_OAUTH_2FA_ENCODING_KEY%/$SPACE_OAUTH_2FA_ENCODING_KEY/" \
        -e "s/%SPACE_OAUTH_ENCODING_KEY%/$SPACE_OAUTH_ENCODING_KEY/" \
        -e "s/%SPACE_OAUTH_MESSAGE_SIGNING_RSA_PUBLIC%/$SPACE_OAUTH_MESSAGE_SIGNING_RSA_PUBLIC/" \
        -e "s/%SPACE_OAUTH_MESSAGE_SIGNING_RSA_PRIVATE%/$SPACE_OAUTH_MESSAGE_SIGNING_RSA_PRIVATE/" \
        -e "s/%SPACE_VCS_SHARED_KEY%/$SPACE_VCS_SHARED_KEY/" \
        -e "s/%VCS_IDENTITY_EMAIL%/$VCS_IDENTITY_EMAIL/" \
        -e "s/%VCS_IDENTITY_SECRETKEY_BASE64%/$VCS_IDENTITY_SECRETKEY_BASE64/" \
        -e "s/%SPACE_WEBHOOK_KEY%/$SPACE_WEBHOOK_KEY/" \
        -e "s/%SPACE_PACKAGES_SHARED_CLIENT_SECRET%/$SPACE_PACKAGES_SHARED_CLIENT_SECRET/" \
        ${shared-config.paths.space}

      SPACE_PACKAGES_SHARED_CLIENT_SECRET=$(read_key "config_package.circlet.packages.oauth.clientSecret")

      sed -i \
        -e "s/%SPACE_PACKAGES_SHARED_CLIENT_SECRET%/$SPACE_PACKAGES_SHARED_CLIENT_SECRET/" \
        ${shared-config.paths.packages}

      SPACE_VCS_SHARED_KEY=$(read_key "config_vcs.circlet.key")
      VCS_SSH_SERVER_KEY_RSA_BASE64=$(read_key "config_vcs.vcs.sshServerKeyRSABase64")
      VCS_GPG_PUBLIC_KEY_BASE64=$(read_key "config_vcs.vcs.gpg.public_key.base64")

      sed -i \
        -e "s/%SPACE_VCS_SHARED_KEY%/$SPACE_VCS_SHARED_KEY/" \
        -e "s/%VCS_SSH_SERVER_KEY_RSA_BASE64%/$VCS_SSH_SERVER_KEY_RSA_BASE64/" \
        -e "s/%VCS_GPG_PUBLIC_KEY_BASE64%/$VCS_GPG_PUBLIC_KEY_BASE64/" \
        ${shared-config.paths.vcs}

      echo "Done"
    '');
  };
in
with lib;
{
  options.services.jetbrains-space = {
    enable = mkEnableOption "JetBrains Space";
    network = {
      dns = mkOption {
        type = types.str;
      };
      domain-name = mkOption {
        type = types.str;
      };
      ip = mkOption {
        type = types.str;
      };
    };
    email = {
      fromAddress = mkOption {
        type = types.str;
      };
      host = mkOption {
        type = types.str;
      };
      port = mkOption {
        type = types.ints.u16;
        default = 25;
      };
      protocol = mkOption {
        type = types.str;
        default = "SMTP";
      };
      login = {
        user = mkOption {
          type = types.str;
        };
        password = mkOption {
          type = types.str;
        };
      };
    };
    paths = {
      access = {
        user = mkOption {
          type = types.str;
          default = default-credentials.user;
        };
        group = mkOption {
          type = types.str;
          default = default-credentials.group;
        };
      };
      logs = mkOption {
        type = types.str;
        default = "/var/log";
      };
      temp = mkOption {
        type = types.str;
        default = "/tmp";
      };
      bulk = mkOption {
        type = types.str;
        default = "/bulk";
      };
      keys = mkOption {
        type = types.str;
        default = "/etc/nixos/src/space-config-keys.json";
      };
    };
    betaCompatible = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    users = with cfg.paths; {
      users = mkIf (access.user == default-credentials.user) {
        "${access.user}" = {
          group = access.group;
          isSystemUser = true;
        };
      };
      groups = mkIf (access.group == default-credentials.group) {
        "${access.group}".name = access.group;
      };
    };

    environment.systemPackages = [ configScript.package ];
  
    systemd.tmpfiles.rules = with builtins; map (
    # TODO: Change back to 0770 once users are sorted (everything other than postgres and elasticsearch uses root, those use different non-root)
      path: "d ${path} 0777 ${cfg.paths.access.user} ${cfg.paths.access.group}"
    ) (
      (attrValues space.paths) ++
      (attrValues postgres.paths) ++
      (attrValues elasticsearch.paths) ++
      (attrValues minio.paths)
    );
  
    virtualisation.oci-containers.backend = "podman";

    virtualisation.oci-containers.containers =
    let
      knowLocalHosts = hosts: (map (name: "--add-host=${name}:${cfg.network.ip}") hosts);
      knowSecondaryHosts = hosts: (map (name: "--add-host=${name}:10.26.0.10") hosts);
      joinNetworks = network-names: (map (network-name: "--network=${network-name}") network-names);
    in {

      init-config = {
        image = "public.registry.jetbrains.space/p/space-on-premises/docker/init-configs:${shared-config.version}";
        environment.AUTOMATION_TAG = shared-config.tag;
        volumes = [ "${shared-config.path}:/home/init-config/config" ];
      };
      space = {
        image = "public.registry.jetbrains.space/p/space-on-premises/docker/space:${space.version}";
        environment.JAVA_OPTS =
          "-Dconfig.file=/home/space/circlet-server-onprem/config/space.on-premises.conf" +
          (if space.betaCompatible then " -Dcirclet.organization.domain=jetbrains" else "");
        extraOptions =
          (knowLocalHosts [ "redis" "postgres" "minio" "elasticsearch" "langservice" "vcs" ]) ++
          (knowSecondaryHosts [ "hub.framebunker-pr5" "gitea.framebunker-pr5" ]) ++
          (joinNetworks [ networks.frontend.name networks.backend.applications.name networks.backend.data.name ]);
        ports = [
          "${toString space.ports.http}:8084/tcp"
          # TODO: Figure this one out. Not described in the compose file, it is needed for VCS to properly auth user git requests
          "9084:9084/tcp"
        ];
        volumes = [ "${space.paths.config}:/home/space/circlet-server-onprem/config" ];
      };
      vcs = {
        image = "public.registry.jetbrains.space/p/space-on-premises/docker/vcs-hosting:${vcs.version}";
        environment.JAVA_OPTS = "-Dproperties.file=config/vcs.on-premises.properties -DVCS_HOSTING_VCS_GIT_MIRROR_ANY=true";
        extraOptions =
          (knowLocalHosts [ "redis" "postgres" "minio" "space" ]) ++
          (joinNetworks [ networks.frontend.name networks.backend.applications.name networks.backend.data.name ]);
        ports = [
          "${toString vcs.ports.http}:8080/tcp"
          "${toString vcs.ports.ssh}:2222/tcp"
        ];
        volumes = [ "${vcs.paths.config}:/home/space/git/vcs-hosting/config" ];
      };
      packages = {
        image = "public.registry.jetbrains.space/p/space-on-premises/docker/packages:${packages.version}";
        environment.JAVA_OPTS = "-Dconfig.file=/home/space/packages-server/config/packages.on-premises.conf";
        extraOptions =
          (knowLocalHosts [ "redis" "postgres" "minio" "elasticsearch" ]) ++
          (joinNetworks [ networks.frontend.name networks.backend.applications.name networks.backend.data.name ]);
        ports = [
          "${toString packages.ports.serviceA}:8390/tcp"
          "${toString packages.ports.serviceB}:9390/tcp"
        ];
        volumes = [ "${packages.paths.config}:/home/space/packages-server/config" ];
      };
      langservice = {
        image = "public.registry.jetbrains.space/p/space-on-premises/docker/langservice:${langservice.version}";
        environment.JAVA_OPTS = "-Dconfig.file=/home/space/langservice-server/config/langservice.on-premises.conf";
        extraOptions = joinNetworks [ networks.backend.applications.name ];
        ports = [ "${toString langservice.ports.service}:8095/tcp" ];
        volumes = [ "${langservice.paths.config}:/home/space/langservice-server/config" ];
      };
      postgres = {
        image = "postgres:${postgres.version}";
        environment = with space.credentials.postgres; {
          POSTGRES_USER = user;
          POSTGRES_PASSWORD = password;
          POSTGRES_DB = database;
        };
        extraOptions = joinNetworks [ networks.backend.data.name ];
        ports = [ "${toString postgres.ports.service}:5432/tcp" ];
        volumes = [ "${postgres.paths.data}:/var/lib/postgresql/data" ];
      };
      elasticsearch = {
        image = "elasticsearch:${elasticsearch.version}";
        environment = {
          ES_JAVA_OPTS = "-Xms512m -Xmx1024m";
          "discovery.type" = "single-node";
          "xpack.security.enabled" = "false";
        };
        extraOptions = joinNetworks [ networks.backend.data.name ];
        ports = with elasticsearch.ports; [
          "${toString serviceA}:9200/tcp"
          "${toString serviceB}:9300/tcp"
        ];
        volumes = [ "${elasticsearch.paths.data}:/usr/share/elasticsearch/data" ];
      };
      redis = {
        image = "redis:${redis.version}";
        extraOptions = joinNetworks [ networks.backend.data.name ];
        ports = [ "${toString redis.ports.service}:6379/tcp" ];
      };
      minio = {
        image = "minio/minio:${minio.version}";
        environment = with space.credentials.minio; {
          MINIO_ROOT_USER = user;
          MINIO_ROOT_PASSWORD = password;
        };
        extraOptions = joinNetworks [ networks.backend.data.name ];
        ports = with minio.ports; [
          "${toString service}:9000/tcp"
          "${toString console}:9001/tcp"
        ];
        cmd = [ "server" "--address" ":9000" "--console-address" ":9001" "--compat" "/data" ];
        volumes = [ "${minio.paths.data}:/data" ];
      };
    };

    services.nginx = {
      enable = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;
      clientMaxBodySize = "0";
      virtualHosts =
      let
        base = locations: {
          inherit locations;
        };
        proxy = port: base {
          "/" = {
            proxyPass = "http://localhost:" + toString (port) + "/";
            proxyWebsockets = true;
          };
        };
        redirect = url: base {
          "/".return = "307 ${url}";
        };
      in
      {
        "space.${cfg.network.domain-name}" = proxy space.ports.http;
      };
    };

    networking.firewall.allowedTCPPorts =
      (builtins.attrValues relay.ports) ++
      (builtins.attrValues space.ports);

    systemd.services.establish-networks = {
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      path = [ pkgs.podman ];
      script = ''
        establish()
        {
          podman network exists $1 || podman network create $1
        }
      
        establish ${networks.frontend.name}
        establish ${networks.backend.applications.name}
        establish ${networks.backend.data.name}
        podman network ls
      '';
    };


    systemd.services.ordered-start = 
    let
      user = "root";
    in {
      serviceConfig = {
        Type = "oneshot";
        User = user;
      };
      path = [ pkgs.nix ];
      environment.NIX_PATH = "nixpkgs=/nix/var/nix/profiles/per-user/${user}/channels/nixos";
      wantedBy = [ "multi-user.target" ];
      after = [ "network-online.target" ];
      script = ''
        wait_for_port()
        {
          echo -n "Wait for port $1"
          while ! (: </dev/tcp/localhost/$1) &> /dev/null; do
            sleep 0.5 && echo -n .
          done
          echo
        }

        wait_for_url()
        {
          echo -n "Wait for URL $1"
          while ! ${pkgs.curl}/bin/curl --output /dev/null --silent --head --fail $1; do
            sleep 0.5 && echo -n .
          done
          echo
        }

        keep_trying_service_start()
        {
          while ! systemctl start $1 --quiet; do
            sleep 5 && echo -n .
          done
          echo
        }

        wait_for_postgres()
        {
          echo -n "Wait for postgres on port $1 - user/db $2"
          while ! ${pkgs.postgresql}/bin/pg_isready --host=localhost --port=$1 --username=$2 --dbname=$2 --quiet; do
            sleep 0.5 && echo -n .
          done
          echo
        }

        wait_for_redis()
        {
          echo -n "Wait for redis on port $1"
          while ! ${pkgs.redis}/bin/redis-cli -h localhost -p $1 ping; do
            sleep 0.5 && echo -n .
          done
          echo
        }

        echo "Beginning ordered start"

        echo "Establishing internal networks..."
        systemctl start --wait establish-networks

        echo "Waiting until successful redis start..."
        keep_trying_service_start podman-redis

        ${configScript.package}/bin/${configScript.name}

        echo "Starting third party services..."
        systemctl start podman-minio
        systemctl start podman-elasticsearch
        systemctl start podman-postgres

        echo "Waiting for redis..."
        wait_for_redis ${toString redis.ports.service}
        echo "Waiting for minio..."
        wait_for_port ${toString minio.ports.service}
        echo "Waiting for elastic search..."
        wait_for_port ${toString elasticsearch.ports.serviceA}
        echo "Waiting for postgres..."
        wait_for_postgres ${toString postgres.ports.service} ${space.credentials.postgres.user}

        echo "Starting JetBrains services..."
        systemctl start podman-packages
        systemctl start podman-langservice

        echo "Waiting for packages..."
        wait_for_port ${toString packages.ports.serviceA}
        echo "Waiting for langservice..."
        wait_for_port ${toString langservice.ports.service}

        echo "Starting Space..."
        systemctl start podman-space

        echo "Blind waiting - for vcs service to connect late..."
        sleep 10

        systemctl start podman-vcs
        sleep 5

        echo "Hard restarting vcs service to have it properly discover its config..."
        systemctl stop podman-vcs
        systemctl start podman-vcs
        
        echo "Waiting for vcs..."
        wait_for_port ${toString vcs.ports.http}

        if [[ -f "${shared-config.paths.extract}" ]]; then
          echo "Existing keys file found at ${shared-config.paths.extract} - skipping extract step"
        else
          echo "Running key extraction until successful..."
          until ${configExtractScript.package}/bin/${configExtractScript.name}; do
            sleep 5
            echo "Retrying extract..."
          done
        fi

        echo "Ordered start complete"
      '';
    };
  };
}
