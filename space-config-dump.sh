#
# Copyright © 2023 framebunker ApS
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 

#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash crudini nodejs nodePackages.npm jq

if [ ! -f node_modules/.bin/parse-hocon ]; then
  # https://github.com/josephtzeng/hocon-parser
  npm install --quiet @pushcorn/hocon-parser 1>&2
fi


# Validate config file paths

configPath=$1

if [ -z "$configPath" ]; then
  configPath=$(pwd)
fi

if [ ! -d "$configPath" ]; then
  echo "Config directory not found: $configPath"
  exit 1
fi

spaceConfigFile="space.on-premises.conf"
packagesConfigFile="packages.on-premises.conf"
vcsConfigFile="vcs.on-premises.properties"

spaceConfigPath="$configPath/$spaceConfigFile"
if [ ! -f "$spaceConfigPath" ]; then
  echo "Space config path not found $spaceConfigPath"
  exit 2
fi

packagesConfigPath="$configPath/$packagesConfigFile"
if [ ! -f "$packagesConfigPath" ]; then
  echo "Packages config path not found $packagesConfigPath"
  exit 3
fi

vcsConfigPath="$configPath/$vcsConfigFile"
if [ ! -f "$vcsConfigPath" ]; then
  echo "VCS config path not found $vcsConfigPath"
  exit 4
fi


# Parsers

function parseHocon {
  local hoconPath=$1
  local variables=("$@")

  local jqParameter=""

  for variable in "${variables[@]:1}"; do
    jqParameter="$jqParameter.$variable, "
  done

  jqParameter=${jqParameter::-2}

  json=$(node_modules/.bin/parse-hocon --no-env --url $hoconPath)

  echo $json | jq -M "$jqParameter"
}

function parseIni {
  local iniPath=$1
  local variables=("$@")

  values=()
  for variable in "${variables[@]:1}"; do
    value=$(crudini --get "$iniPath" "" "$variable")
    values+=($value)
  done

  for value in "${values[@]}"; do
    echo "\"$value\""
  done
}


# Declare variables

spaceConfigVariables=(
  "circlet.masterSecret"
  "circlet.oauth.accessToken.rsa.public"
  "circlet.oauth.accessToken.rsa.private"
  "circlet.oauth.message.encoding.key"
  "circlet.oauth.twoFactor.encryptionKey"
  "circlet.oauth.encryptionKey"
  "circlet.oauth.messageSigning.rsa.public"
  "circlet.oauth.messageSigning.rsa.private"
  "circlet.vcs.secret"
  "circlet.vcs.identity.email"
  "circlet.vcs.identity.secretKey_base64"
  "circlet.webhooks.key"
  "circlet.packages.oauth.clientSecret"
)

packageConfigVariables=(
  "circlet.packages.oauth.clientSecret"
)

vcsConfigVariables=(
  "circlet.key"
  "vcs.sshServerKeyRSABase64"
  "vcs.gpg.public_key.base64"
)


# Read values

spaceConfigValues=()
while IFS= read -r value; do
  if [[ "$value" =~ ^"%[A-Z_]*%"$ ]]; then
    >&2 echo "Bailing on template value in $spaceConfigPath: $value"
    exit 5
  fi
  spaceConfigValues+=( "$value" )
done < <( parseHocon "$spaceConfigPath" "${spaceConfigVariables[@]}" )

packageConfigValues=()
while IFS= read -r value; do
  if [[ "$value" =~ ^"%[A-Z_]*%"$ ]]; then
    >&2 echo "Bailing on template value in $packagesConfigPath: $value"
    exit 6
  fi
  packageConfigValues+=( "$value" )
done < <( parseHocon "$packagesConfigPath" "${packageConfigVariables[@]}" )

vcsConfigValues=()
while IFS= read -r value; do
  if [[ "$value" =~ ^"%[A-Z_]*%"$ ]]; then
    >&2 echo "Bailing on template value in $vcsConfigPath: $value"
    exit 7
  fi
  vcsConfigValues+=( "$value" )
done < <( parseIni "$vcsConfigPath" "${vcsConfigVariables[@]}" )


# Collect in json format

result="{}"

for (( index=0; index<${#spaceConfigVariables[@]}; index++ )); do
  variable=${spaceConfigVariables[$index]}
  value=${spaceConfigValues[$index]}
  arguments=".config_space.$variable=$value"
  result=$(echo "$result" | jq "$arguments")
done

for (( index=0; index<${#packageConfigVariables[@]}; index++ )); do
  variable=${packageConfigVariables[$index]}
  value=${packageConfigValues[$index]}
  arguments=".config_package.$variable=$value"
  result=$(echo "$result" | jq "$arguments")
done

for (( index=0; index<${#vcsConfigVariables[@]}; index++ )); do
  variable=${vcsConfigVariables[$index]}
  value=${vcsConfigValues[$index]}
  arguments=".config_vcs.$variable=$value"
  result=$(echo "$result" | jq "$arguments")
done


# We are done - make it nice / pipe it out

echo $result | jq
